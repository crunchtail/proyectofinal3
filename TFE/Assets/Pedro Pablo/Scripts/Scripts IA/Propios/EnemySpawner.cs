﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{   public GameObject enemy;
    public GameObject flyer;
    public GameObject SpawnPoint;
    public int zombie_counter;
    public int flyer_counter;

    public GameObject[] buscador_zombie;
    public GameObject[] buscador_flyer;

     
    // Start is called before the first frame update
    void Start()
    {
        Spawner();
    }

    // Update is called once per frame
    void Update()
    {
        buscador_zombie = GameObject.FindGameObjectsWithTag("Enemy");
        buscador_flyer = GameObject.FindGameObjectsWithTag("Fly");

        if((buscador_zombie.Length == 0) && (buscador_flyer.Length == 0))
        {
            Debug.Log("No Enemies");
            zombie_counter = zombie_counter + (zombie_counter / 4);
            flyer_counter = flyer_counter + (flyer_counter / 4);

            Spawner();
        }
    }
    
    public void Spawner()
    {

        for (int i = 0; i < zombie_counter; i++)
        {
            Instantiate(enemy, SpawnPoint.transform.position, Quaternion.identity);        
        }

        for (int i = 0; i < flyer_counter; i++)
        {
            Instantiate(flyer, SpawnPoint.transform.position, Quaternion.identity);
        }
    }


}
