﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class Agent : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target;
    NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //Enemy_Finder = GameObject.FindGameObjectsWithTag("heart");
        //BuscarEnemigo();
    }

    // Update is called once per frame
    void Update()
    {
        if (target)
        {
            agent.SetDestination(target.transform.position);
        }
        else
        {
            Debug.Log("No Target");
        }
            
    }

    
}
