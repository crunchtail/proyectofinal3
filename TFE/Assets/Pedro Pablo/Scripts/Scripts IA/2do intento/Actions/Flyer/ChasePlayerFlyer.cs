﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChasePlayerFlyer : State
{
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public bool IsPursuit;
    public Animator anim;
    public NavMeshAgent NavM;
    public float MinDist;
    public GameObject target;
    public GameObject capsule;

    public void Update(GameObject npc)
    {
        anim.SetBool("IsPursuit", IsPursuit);
        npc.GetComponent<Flyer>().capsule.transform.rotation = capsule.transform.rotation;
    }

    public ChasePlayerFlyer()
    {
        stateID = StatesID.ChasePlayerFlyer;

    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (distanceToPlayer <= MinDist)
            npc.GetComponent<Flyer>().SetTransition(TransitionID.ToAttackFlyer);
        //else
        //    npc.GetComponent<Flyer>().SetTransition(TransitionID.ToPatrol);

    }

    public override void Act(GameObject player, GameObject npc)
    {
        target = npc.GetComponent<Flyer>().player;
        Debug.Log("ChasePlayerFlyer");
        capsule = npc.GetComponent<Flyer>().capsule;
        Vector3 PointAtLook = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
        capsule.transform.LookAt(PointAtLook);
        IsPursuit = true;

        //npc.GetComponent<Flyer>().IsPursuit = IsPursuit;

        NavM = npc.GetComponent<NavMeshAgent>();
        NavM.speed = 5;
        MinDist = NavM.stoppingDistance*2;

        npc.GetComponent<Flyer>().minAtkDistance = MinDist;

        minPlayerDetectDistance = npc.GetComponent<Flyer>().minPlayerDetectDistance;
        distanceToPlayer = npc.GetComponent<Flyer>().distanceToPlayer;
    }
}
