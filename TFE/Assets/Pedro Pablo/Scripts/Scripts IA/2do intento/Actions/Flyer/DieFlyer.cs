﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieFlyer : State
{
    public float vida;
    public void Update()
    {
        
    }

    public DieFlyer(float vida, GameObject npc)
    {
        stateID = StatesID.DieFlyer;
        vida = npc.GetComponent<Flyer>().vida;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (vida <= 0) ;

    }

    public override void Act(GameObject player, GameObject npc)
    {
         //Destroy(npc);
    }

}
