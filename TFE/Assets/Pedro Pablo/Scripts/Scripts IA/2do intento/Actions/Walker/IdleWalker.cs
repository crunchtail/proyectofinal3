﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleWalker : State
{
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public void Update()
    {
        
    }

    public IdleWalker(GameObject npc)
    {
        stateID = StatesID.IdleWalker;
    }

    public override void Reason(GameObject player, GameObject npc)
    {
       if(distanceToPlayer<= minPlayerDetectDistance)
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToChasePlayerWalker);

    }

    public override void Act(GameObject player, GameObject npc)
    {
        Debug.Log("IdleWalker");
        //Destroy(npc);
        minPlayerDetectDistance = npc.GetComponent<Walker>().minPlayerDetectDistance;
        distanceToPlayer = npc.GetComponent<Walker>().distanceToPlayer;
    }

}
