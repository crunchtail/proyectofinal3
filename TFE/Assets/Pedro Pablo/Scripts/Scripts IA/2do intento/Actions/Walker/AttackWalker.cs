﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackWalker : State
{
    public Animator anim;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public float AtkDistance;
    public Vector3 rayDirection;
    public bool canAtk;
    public bool IsPursuit;
    public bool IsAtk;
    public NavMeshAgent NavM;
    public Agent agente;
    public Collider coll;
    public GameObject target;

    public void Update(GameObject npc)
    {

        anim.SetBool("IsPursuit", IsPursuit);
        anim.SetBool("IsAtk", IsAtk);
    }

    public AttackWalker()
    {
        stateID = StatesID.AttackWalker;
        

    }

    public override void Reason(GameObject player, GameObject npc)
    {
        

        if (distanceToPlayer > AtkDistance)
        {

            //canAtk = false;

            IsAtk = false;
            IsPursuit = true;
            NavM.enabled = true;
            agente.enabled = true;

            npc.GetComponent<Walker>().IsAtk = IsAtk;
            npc.GetComponent<Walker>().IsPursuit = IsPursuit;
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToChasePlayerWalker);
        }

        if(target == null)
        {
            IsAtk = false;
            IsPursuit = true;
            NavM.enabled = true;
            agente.enabled = true;
            npc.GetComponent<Walker>().IsAtk = IsAtk;
            npc.GetComponent<Walker>().IsPursuit = IsPursuit;
            npc.GetComponent<Walker>().SetTransition(TransitionID.ToFindPlayerWalker);
        }

    }

    public override void Act(GameObject player, GameObject npc)
    {
        AtkDistance = npc.GetComponent<Walker>().minAtkDistance;
        target = npc.GetComponent<Walker>().player;
        NavM = npc.GetComponent<NavMeshAgent>();
        agente = npc.GetComponent<Agent>();
        
        Debug.Log("AttackWalker");


            //canAtk = true;

        IsAtk = true;
        IsPursuit = false;
        NavM.enabled = false;
        agente.enabled = false;

        npc.GetComponent<Walker>().IsAtk = IsAtk;
        npc.GetComponent<Walker>().IsPursuit = IsPursuit;


        minPlayerDetectDistance = npc.GetComponent<Walker>().minPlayerDetectDistance;
        distanceToPlayer = npc.GetComponent<Walker>().distanceToPlayer;
    }


}
