﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour
{
    #region public variables
    public float life;
    public float velocity;
    public bool isAlive = true;
    #endregion

    #region public methods
    void Start()
    {

    }

    void Update()
    {

    }

    public virtual void Move()
    {

    }

    public virtual void ApplyDamage()
    {

    }
    #endregion

}
