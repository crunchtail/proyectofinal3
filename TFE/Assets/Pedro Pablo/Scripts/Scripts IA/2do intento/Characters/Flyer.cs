﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flyer : MonoBehaviour
{
    #region public variables
    public Animator anim;
    public GameObject player;
    public GameObject bullet;
    public Transform spawner;
    public float vida;
    public float vidaMax;
    public Transform[] path;
    public NavMeshAgent NavM;
    public Agent agente;
    public GameObject npc;
    public GameObject capsule;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public float minAtkDistance;
    public Vector3 rayDirection;
    public bool canAtk;
    public GameObject[] Enemy_Finder;
    public List<GameObject> visible_enemy;
    public bool IsAtk;
    public bool IsPursuit;
    public LayerMask PlayerLayer;

    #endregion

    #region private variables
    private StateMachine stateMachine;
    #endregion


    #region methods
    public void SetTransition(TransitionID t)
    {
        stateMachine.PerformTransition(t);
    }

    private void MakeFSM()
    {
        FollowPath stateFollowPath = new FollowPath(npc);
        stateFollowPath.AddTransition(TransitionID.ToChasePlayerFlyer, StatesID.ChasePlayerFlyer);

        ChasePlayerFlyer stateChasePlayer = new ChasePlayerFlyer();
        stateChasePlayer.AddTransition(TransitionID.ToAttackFlyer, StatesID.AttackFlyer);

        FindPlayerFlyer stateFindPlayer = new FindPlayerFlyer();
        stateFindPlayer.agente = agente;
        stateFindPlayer.AddTransition(TransitionID.ToChasePlayerFlyer, StatesID.ChasePlayerFlyer);

        AttackFlyer stateAttackFlyer = new AttackFlyer();
        stateAttackFlyer.AddTransition(TransitionID.ToChasePlayerFlyer, StatesID.ChasePlayerFlyer);
        stateAttackFlyer.AddTransition(TransitionID.ToFindPlayerFlyer, StatesID.FindPlayerFlyer);

        stateMachine = new StateMachine();
        stateMachine.AddState(stateFindPlayer);
        stateMachine.AddState(stateFollowPath);
        stateMachine.AddState(stateChasePlayer);
        stateMachine.AddState(stateAttackFlyer);
        

    }


    void Start()
    {
        NavM = GetComponent<NavMeshAgent>();
        MakeFSM();
        npc = this.gameObject;
        anim = GetComponent<Animator>();
        vida = vidaMax;
        IsAtk = false;
    }

    void FixedUpdate()
    {
        stateMachine.CurrentState.Reason(player, this.gameObject);
        stateMachine.CurrentState.Act(player, this.gameObject);
    }

    void Update()
    {
        RaycastHit hit;
        if (player)
        {
            Vector3 rayDirection = player.transform.position - npc.transform.position;

            if (Physics.Raycast(npc.transform.position, rayDirection, out hit, Mathf.Infinity, PlayerLayer))
            { // If the enemy is very close behind the player and in view the enemy will detect the player

                Debug.DrawRay(npc.transform.position, rayDirection, Color.red);


                distanceToPlayer = hit.distance;
            }

            //anim.SetBool("IsPursuit", IsPursuit);
            anim.SetBool("IsAtk", IsAtk);
        }
        else
        {
            Debug.Log("NoTargetForRayCast");
        }



        if (vida <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void Atacar()
    {
        GameObject bala = Instantiate(bullet, capsule.transform.position, Quaternion.identity);
        

    }

    #endregion
}
