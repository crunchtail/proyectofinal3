﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Walker : MonoBehaviour
{
    #region public variables
    public Animator anim;
    public float distanceToPlayer;
    public float minPlayerDetectDistance;
    public float minAtkDistance;

    public GameObject player;
    public Vector3 rayDirection;
    public bool canAtk;
    public GameObject[] Enemy_Finder;
    public List<GameObject> visible_enemy;
    public Agent agente;
    public NavMeshAgent NavM;
    public GameObject npc;
    public LayerMask playerLayer;



    public float vida;
    public float vida_max;
    public float atk;
    public float speed;
    public Collider coll;


    public bool IsPursuit;
    public bool IsAtk;
    #endregion

    #region private variables
    private StateMachine stateMachine;
    #endregion


    #region methods
    public void SetTransition(TransitionID t)
    {
        stateMachine.PerformTransition(t);
    }

    private void MakeFSM()
    {
        ChasePlayerWalker stateChasePlayer = new ChasePlayerWalker();
        stateChasePlayer.AddTransition(TransitionID.ToAttackWalker, StatesID.AttackWalker);
        stateChasePlayer.AddTransition(TransitionID.ToIdleWalker, StatesID.IdleWalker);

        IdleWalker stateIdle = new IdleWalker(npc);
        stateIdle.AddTransition(TransitionID.ToChasePlayerWalker, StatesID.ChasePlayerWalker);

        FindPlayerWalker stateFindPlayer = new FindPlayerWalker();
        stateFindPlayer.agente = agente;
        stateFindPlayer.AddTransition(TransitionID.ToIdleWalker, StatesID.IdleWalker);

        AttackWalker stateAttackWalker = new AttackWalker();
        stateAttackWalker.AddTransition(TransitionID.ToChasePlayerWalker, StatesID.ChasePlayerWalker);
        stateAttackWalker.AddTransition(TransitionID.ToFindPlayerWalker, StatesID.FindPlayerWalker);

        stateMachine = new StateMachine();
        
        stateMachine.AddState(stateFindPlayer);
        stateMachine.AddState(stateChasePlayer);
        stateMachine.AddState(stateIdle);
        stateMachine.AddState(stateAttackWalker);
        
    }


    void Start()
    {
        MakeFSM();
        npc = this.gameObject;
        anim = GetComponent<Animator>();
        vida = vida_max;
    }

    void FixedUpdate()
    {
        stateMachine.CurrentState.Reason(player, this.gameObject);
        stateMachine.CurrentState.Act(player, this.gameObject);
    }

    void Update()
    {
       

        RaycastHit hit;
        if (player)
        {
            Vector3 rayDirection = player.transform.position - npc.transform.position;

            if (Physics.Raycast(npc.transform.position, rayDirection, out hit, Mathf.Infinity, playerLayer))
            { // If the enemy is very close behind the player and in view the enemy will detect the player

                Debug.DrawRay(npc.transform.position, rayDirection, Color.red);


                distanceToPlayer = hit.distance;
            }

            anim.SetBool("IsPursuit", IsPursuit);
            anim.SetBool("IsAtk", IsAtk);
        }
        else
        {
            Debug.Log("NoTargetForRayCast");
        }



        if (vida <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void Ataque()
    {
        coll.enabled = true;


    }

    public void AtaqueOff()
    {
        coll.enabled = false;
    }

    #endregion
}
