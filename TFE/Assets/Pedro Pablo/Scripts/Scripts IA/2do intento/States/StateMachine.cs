﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{
    private List<State> states;

    private StatesID currentStateID;
    public StatesID CurrentStateID { get { return currentStateID; } }

    private State currentState;
    public State CurrentState { get { return currentState; } }

    //Constructor
    public StateMachine()
    {
        states = new List<State>();
    }

    public void AddState(State s)
    {
        if (s == null)
            return;

        for (int i = 0; i < states.Count; i++)
        {
            if (states[i].ID == s.ID)
            {
                return;
            }
        }

        if (states.Count == 0)
        {
            states.Add(s);
            currentState = s;
            currentStateID = s.ID;
            return;
        }

        states.Add(s);
    }

    public void DeleteStates(State s)
    {
        if (s.ID == StatesID.NullState)
            return;

        for (int i = 0; i < states.Count; i++)
        {
            if (states[i].ID == s.ID)
            {
                states.Remove(s);
                return;
            }
        }
    }

    public void PerformTransition(TransitionID transition)
    {
        if (transition == TransitionID.NullTransition)
            return;

        StatesID destinationStateID = currentState.GetOutputState(transition);
        if (destinationStateID == StatesID.NullState)
            return;

        currentStateID = destinationStateID;

        for (int i = 0; i < states.Count; i++)
        {
            if (states[i].ID == currentStateID)
            {
                currentState.DoBeforeLeaving();                     

                currentState = states[i];

                currentState.DoBeforeEntering();
            }
        }
    }

}

