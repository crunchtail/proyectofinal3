﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    protected Dictionary<TransitionID, StatesID> map = new Dictionary<TransitionID, StatesID>();
    protected StatesID stateID;
    public StatesID ID { get { return stateID; } }

    //Añadimos transicion a la maquina de estados
    public void AddTransition(TransitionID transId, StatesID stateId)
    {
        if (transId == TransitionID.NullTransition)
        {
            return;
        }

        if (stateId == StatesID.NullState)
        {
            return;
        }

        if (map.ContainsKey(transId))
        {
            return;
        }
        map.Add(transId, stateId);
    }

    //Eliminamos transicion de la maquina de estados
    public void RemoveTransition(TransitionID transId)
    {
        if (transId == TransitionID.NullTransition)
        {
            return;
        }

        if (map.ContainsKey(transId))
        {
            map.Remove(transId);
        }
    }

    //Recogemos el estado de salida
    public StatesID GetOutputState(TransitionID transID)
    {
        if (map.ContainsKey(transID))
        {
            return map[transID];
        }

        return StatesID.NullState;
    }

    //Hacemos algo antes de entrar a un nuevo estado
    public virtual void DoBeforeEntering()
    {

    }

    //Hacemos algo después de salir de un estado
    public virtual void DoBeforeLeaving()
    {

    }

    //Comprobamos si pasamos a otro estado
    public abstract void Reason(GameObject p, GameObject n);

    //Codigo que se ejecuta en el estado actual
    public abstract void Act(GameObject p, GameObject n);
}
