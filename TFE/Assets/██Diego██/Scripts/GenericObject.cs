﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GenericObject : MonoBehaviour,Idamageable
{
    Rigidbody rb;
    public void damage(float daño, Vector3 PointImpact)
    {
        rb.AddForce(PointImpact, ForceMode.Force);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
