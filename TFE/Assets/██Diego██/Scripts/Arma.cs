﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : Objeto
{

    Animator animPersonaje;
    public GameObject particulas;
    RaycastHit hit;
    public Transform camera, mirilla, posicionBala;
    public IEnumerator coroutine;


    public float daño;
    public float cadencia;

    private int balasEnElCargador;
    public int balasBolsillo;
    public int balasMaxCargador;
    public int balasMaxBolsillo;
    public bool canShoot;
    public bool isReloading;

    public bool LockGun;

    public Arma cambiarArma()
    {
        LockGun = true;
        //StartCoroutine(desbloqueoelarma(x));
        return this;
    }
    public int BalasEnElCargador { get => balasEnElCargador; set {
            if(value == 0)
            {
				
                Recargar();
                //InventarioGUI.singleton.ActualizaMunicion(value);
            }
            balasEnElCargador = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        identificador = "Arma";
        canShoot = true;
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Transform>();
        mirilla = GameObject.FindGameObjectWithTag("Mirilla").GetComponent<Transform>();
        balasBolsillo = balasMaxBolsillo;
        balasEnElCargador = balasMaxCargador;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (BalasEnElCargador <= 0)
        {
            canShoot = false;
            if (balasBolsillo > 0)
            {
                Recargar();
            }


        }
        #region apuntado
        Ray rayo = new Ray();
        rayo.direction = camera.transform.forward;
        if (Physics.Raycast(camera.position, camera.forward, out hit, 50))
        {
            mirilla.position = hit.point;
        }
        else
        {
            mirilla.localPosition = new Vector3(0, 0, 50);
        }

        posicionBala.LookAt(mirilla);
#endregion
    }
    virtual public void Disparar()
    {

    }
    public bool CheckValue(int value)
    {
        if (isReloading) return false;
        if (LockGun) return false;
        if (value <= 0)
        {
            return true;
        }
        return false;
    }
    public virtual void Recargar()
    {
        //if (CheckValue(BalasEnElCargador)==true) return;
        
        coroutine = RecuperarBalas();
        StartCoroutine(coroutine);
        animPersonaje.SetBool("Recargar", true);
    }
    public void BloqueoCargador() { }
    public void SueltoCargador() { }
    public void RecargoElCargador() { }
    public override void Usar(Animator anim, PlayerController controller)
    {
        // si no tiene mu nicion retorna
        if (animPersonaje == null)
        {
            animPersonaje = anim;
        }
        if (canShoot && !isReloading && !animPersonaje.GetCurrentAnimatorStateInfo(2).IsTag("Recargar"))
        {
            anim.SetTrigger("Shoot");
            balasEnElCargador -= 1;
            canShoot = false;
            StartCoroutine(CanShootAgain());


            if (Physics.Raycast(posicionBala.position, posicionBala.forward, out hit, 50))
            {
                Idamageable idam = hit.collider.GetComponent<Idamageable>();
                if ( idam!= null)
                {
                    idam.damage(daño, hit.point);
                }
                Debug.Log(hit.collider.gameObject);
                Instantiate(particulas, hit.point, Quaternion.identity);


            }
        }
        if(balasBolsillo > 0 && balasEnElCargador <= 0)
        {
            Debug.Log("Recargaaa");
            
            Recargar();
        }
    }
    IEnumerator CanShootAgain()
    {
        yield return new WaitForSeconds(cadencia);
        if (balasEnElCargador > 0)
        {
            canShoot = true;
        }

    }
    IEnumerator RecuperarBalas()
    {
        isReloading = true;
        yield return new WaitForSeconds(2f);
        animPersonaje.SetBool("Recargar", false);
        for (int i = 0; i < balasMaxCargador; i++)
        {
            if (balasBolsillo > 0)
            {
                if (balasEnElCargador < balasMaxCargador)
                {
                    balasEnElCargador++;
                    balasBolsillo--;
                }
            }
        }
        isReloading = false;


        canShoot = true;
        


    }


}
