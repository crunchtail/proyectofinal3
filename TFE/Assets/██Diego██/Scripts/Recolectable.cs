﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recolectable : Bolt.GlobalEventListener
{
    public GameObject prefabRecolectable;
	public Bolt.PrefabId boltId;
	public bool recogido;
	BoltEntity entity;
	
    
    // Start is called before the first frame update


    //SOY UN M4
    



    void Start()
    {

		entity = GetComponent<BoltEntity>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Recoger(GameObject objetoParaRecoger, Inventary inventario)
    {
		
		if (!recogido)
		{
			inventario.AñadirObjeto(objetoParaRecoger);
			DestruirObjeto();

		}
		recogido = true;
		

		


	}
	public void DestruirObjeto()
	{

		Debug.Log("Enviando Mensaje de Borrado");
		var msg = ObjetoRecogido.Create();
		msg.objectEntity = entity;
		msg.Send();
	}
	public override void OnEvent(ObjetoRecogido evnt)
	{
		Debug.Log("Borrando objeto de la escena");
		if (BoltNetwork.IsServer)
		{
			Debug.Log("Destruyendo objeto desde el servidor");
			if(evnt.objectEntity != null)
			{
				BoltNetwork.Destroy(evnt.objectEntity.gameObject);
			}
			else
			{
				Debug.Log("El objeto fue destruido");
			}
			
		}
	}
}
