﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PhotonLoader : Bolt.GlobalEventListener
{
    [SerializeField]
    public PlayerController[] players;
    
    BoltEntity jugador;
	BoltEntity inventario;
    BoltEntity servidor;

    spawnPointsManager spawns;

    public bool isReady;

	ControlRotCamera rotCamera;

	FollowTargetSmooth targetCamara;

	int nJugadores;

	int dictionaryIndex = 0;
	PlayerController[] jugadores = new PlayerController[4];

	public GameObject canvasLoader;


	public override void SceneLoadLocalDone(string scene)
    {

        Debug.Log("Diego:escena cargada en red");
		
        if (BoltNetwork.IsServer)
        {
			//instancia los objetos del escenario que sean online
			IniciarPartida();

			

		}
        //sea servidor o no se incia el cliente, para crear su propio player
        StartCoroutine(iniciarClienteLocal());
		


	}
	public override void SceneLoadRemoteDone(BoltConnection connection)
	{

		if (BoltNetwork.IsServer)
		{
			
			
			base.SceneLoadRemoteDone(connection);
			//crear inventario para el jugador conectado
			
			
		}



	}
	// Update is called once per frame
	void Update()
    {
		if (Input.GetKeyDown(KeyCode.K))
		{
			Debug.Log(nJugadores);
		}
    }

    private void DebugInfoPhoton()
    {
        Debug.Log("Network Status: Is server: " + BoltNetwork.IsServer);
        Debug.Log("Network Status: Is client: " + BoltNetwork.IsClient);

    }
    public void IniciarPartida()
    {

        DebugInfoPhoton();
		

		
        servidor = BoltNetwork.Instantiate(BoltPrefabs.Gamemanager, Vector3.zero, Quaternion.identity);
            


		BoltNetwork.Instantiate(BoltPrefabs.Pocion_recogible, Vector3.zero, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.Pocion_recogible, Vector3.zero + Vector3.right*2, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.Pocion_recogible, Vector3.zero + Vector3.right * 4, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.m4a1Recogible, Vector3.zero - Vector3.right * 2, Quaternion.identity);
		BoltNetwork.Instantiate(BoltPrefabs.m4a1Recogible, Vector3.zero - Vector3.right * 4, Quaternion.identity);

		spawns = FindObjectOfType<spawnPointsManager>();
		Debug.Log("Diego:Iniciando manager de partida");
        

       
        
        
		
        
        

		

           
        
    }

    IEnumerator iniciarClienteLocal()
    {

        GameObject item;
        do
        {
            item = GameObject.FindGameObjectWithTag("Gamemanager");

            if (item != null)   servidor = item.GetComponent<BoltEntity>();

            yield return new WaitForEndOfFrame();
        } while (servidor == null);

		

        
		//encontrar la camara en la escena
		targetCamara = FindObjectOfType<FollowTargetSmooth>();

		rotCamera = FindObjectOfType<ControlRotCamera>();

		//GUI
		InventarioGUI scriptGUI = FindObjectOfType<InventarioGUI>();
		Vector3 spawnPosition;
		
		while (servidor.GetState<IGameManager>().Spawns == null)
		{
			
			yield return new WaitForEndOfFrame();
		}
		nJugadores = servidor.GetState<IGameManager>().NumeroPlayers;
		spawnPosition = servidor.GetState<IGameManager>().Spawns[nJugadores];
		Debug.Log("Soy el jugador " + nJugadores  + "  ");
	
		
		jugador = BoltNetwork.Instantiate(BoltPrefabs.Player, spawnPosition, Quaternion.identity);

		SpawnPlayer();
		canvasLoader.SetActive(false);

		
	}
	public void SpawnPlayer()
	{
		Debug.Log("Spawning player");
		Debug.Log(spawns);
		

		//for (int i = 0; i < 4; i++)
		//{
		//	if (jugadores[i] == null)
		//	{
		//		jugadores[i] = jugador.GetComponent<PlayerController>();
		//		break;
		//	}
		//}
		rotCamera.mianim = jugador.GetComponent<PlayerController>().anim;

		

		targetCamara.target = jugador.GetComponent<Transform>();

		var msg = AumentarJugadores.Create();
		
		msg.Send();
		jugador.TakeControl();
	}

        
    public override void OnEvent(AumentarJugadores evnt)
    {
		
        
        if (!BoltNetwork.IsServer) return;
        Debug.Log("Evento de player creado");
        servidor.GetState<IGameManager>().NumeroPlayers++;

		


		
		//servidor.GetComponent<LevelDataManager>().inventarios[servidor.GetState<IGameManager>().NumeroPlayers] = inventario.GetComponent<Inventary>();
		//for (int i = 0; i < 4; i++)
		//{
		//	if (jugadores[i] != null)
		//	{
		//		jugadores[i] = jugador.GetComponent<PlayerController>();
		//		jugadores[i].inventario = inventario.GetComponent<Inventary>();
		//		inventario.GetComponent<Inventary>().player = jugadores[i].gameObject;
		//		break;
		//	}
		//}

	}

}
