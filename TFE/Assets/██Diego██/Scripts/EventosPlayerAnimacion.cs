﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventosPlayerAnimacion : MonoBehaviour
{
    //Animato
    public Animator mianim;

    //cosas para M4A1
    public Transform posicionBala;
    public GameObject particulas;
    public int balasMax_M4 = 10;
    public int balascargador_M4 = 10;
    public int balasbolsillo_M4;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {



        mianim.SetInteger("balascargador_M4", balascargador_M4);

    }

    public void UsarM4A1()
    {
        //y aqui va a pasar siempre
        balascargador_M4--;

        //todo esto va a pasar si choca
        RaycastHit hit;
        if (Physics.Raycast(posicionBala.position, posicionBala.forward, out hit, 50))
        {
           
                Debug.Log(hit.collider.gameObject);
                Instantiate(particulas, hit.point, Quaternion.identity);
            

        }

    }
}
