﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// This script moves the character controller forward
// and sideways based on the arrow keys.
// It also jumps when pressing space.
// Make sure to attach a character controller to the same game object.
// It is recommended that you make only one call to Move or SimpleMove per frame.

public class PlayerController : Bolt.EntityBehaviour<IPlayer>
{
    CharacterController characterController;
    //Estadisticas Player
    public float speed = 6.0f;
    public float vida = 100;
	public float vidaMax = 100;
	public int puntos = 0;

    float running = 1;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float rot_speed = 3;
    private Vector3 moveDirection = Vector3.zero;

    public Animator anim;
    public Transform posicionObjetos;
    

    bool jump;
	public bool isShopping;
    float xRoot;

    public Objeto objetoAtual;
	public Arma armaActual;
	public Consumible consumibleActual;
    public LayerMask objetosRecogibles;
    public Inventary inventario;
    public Text balasCargador, balasBolsillo, puntosText;
	



    void Start()
    {
		
        Objeto p = new Objeto();
        Arma act = new Arma();
		
		

		if (entity.IsOwner)
        {
			FindObjectOfType<InventarioGUI>().inventaryData = this.GetComponent<Inventary>();
			
			FindObjectOfType<AimingCamera>().posicionBala = posicionObjetos.Find("posicionApuntado");
			
            
        }
        if (p.GetType() == act.GetType())
        {
            act.Recargar();
        }
    }

    private void Update()
    {
		#region server
		//Deteccion de objetos
		if (entity.IsOwner)
		{
			Debug.DrawRay(transform.position + Vector3.up * 0.2f, transform.forward * 1f, Color.red);
			RaycastHit hit;
			if (Physics.Raycast(transform.position + Vector3.up * 0.2f, transform.forward, out hit, 1, objetosRecogibles, QueryTriggerInteraction.Collide))
			{
				
				if (hit.collider.gameObject.GetComponent<Recolectable>() != null)
				{
					anim.ResetTrigger("Shoot");
					
					if (hit.collider.gameObject.GetComponent<Recolectable>().recogido == false && !inventario.IsFull())
					{

						//GameObject nuevaArma = Instantiate(hit.collider.gameObject.GetComponent<Recolectable>().prefabRecolectable, posicionObjetos);
						GameObject nuevaArma = BoltNetwork.Instantiate(hit.collider.GetComponent<Recolectable>().boltId);
						nuevaArma.transform.SetParent(posicionObjetos);
						nuevaArma.transform.localPosition = Vector3.zero;
						////nuevaArma.transform.rotation = Quaternion.identity;
						nuevaArma.GetComponent<Objeto>().inventario = inventario;


						hit.collider.gameObject.GetComponent<Recolectable>().Recoger(nuevaArma, inventario);
						hit.collider.gameObject.GetComponent<Recolectable>().DestruirObjeto();
						
					}




				}
			}

		}
		#endregion
		#region sync for not owners

		if (!entity.IsOwner)
        {
            if (characterController != null)
            {
                characterController.enabled = false;
                
            }

            anim.SetBool("isRunning", state.isRunning);
            anim.SetBool("isGrounded", state.isGrounded);
            anim.SetFloat("Horizontal", state.Horizontal);
            anim.SetFloat("GradosX", state.GradosX);
            anim.SetFloat("Vertical", state.Vertical);
            

            return;
        }
		#endregion
		#region localData
		if (balasCargador == null)
        {
            balasCargador = GameObject.FindGameObjectWithTag("BalasCargador").GetComponent<Text>();
        }
        if (balasBolsillo == null)
        {
            balasBolsillo= GameObject.FindGameObjectWithTag("BalasBolsillo").GetComponent<Text>();
        }
		
        if (inventario.objetosInventario[inventario.index] != null)
        {
            objetoAtual = inventario.objetosInventario[inventario.index].GetComponent<Objeto>();
        }
		else
		{
			objetoAtual = null;
			armaActual = null;
			consumibleActual = null;
		}
		if(puntosText == null)
		{
			puntosText = GameObject.FindGameObjectWithTag("Puntos").GetComponent<Text>();
		}
		else
		{
			puntosText.text = puntos.ToString();
			
		}
		
        //Informacion del arma en la GUI
        if(objetoAtual != null)
        {
                        
			if(objetoAtual.identificador == "Arma")
			{
				armaActual = objetoAtual.GetComponent<Arma>();
				consumibleActual = null;
				balasCargador.gameObject.SetActive(true);
				balasBolsillo.gameObject.SetActive(true);

				balasBolsillo.text = objetoAtual.GetComponent<Arma>().balasBolsillo.ToString();
				balasCargador.text = objetoAtual.GetComponent<Arma>().BalasEnElCargador.ToString();
			}
			if(objetoAtual.identificador == "Consumible")
			{
				consumibleActual = objetoAtual.GetComponent<Consumible>();
				armaActual = null;
			}
        }
        else
        {
            balasCargador.gameObject.SetActive(false);
            balasBolsillo.gameObject.SetActive(false);
        }
		#endregion


		#region Inputs
		//Inputs
		if (characterController.isGrounded && !isShopping)
        {
            //Animator
            anim.SetBool("isGrounded", true);
            anim.SetFloat("Vertical", Input.GetAxis("Vertical"));
            
            anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));

            //Movimiento
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
			moveDirection = moveDirection.normalized;
			moveDirection *= speed;
            if (Input.GetButton("Fire3"))
            {
                anim.SetBool("isRunning", true);
                running = 2;
            }
            else
            {
                anim.SetBool("isRunning", false);
                running = 1;
            }

            if (Input.GetButton("Fire2") || Input.GetKey(KeyCode.P))
            {
				if(armaActual != null)
				{
					anim.SetLayerWeight(1, 1);
				}
                
            }
            else
            {
                anim.SetLayerWeight(1, 0);
            }
            if (Input.GetButton("Fire1"))
            {
                if (objetoAtual != null)
                {
                    
                    
                     //anim.SetTrigger("Shoot");
                     //shoot.Disparar();
                     objetoAtual.Usar(anim, this);
                    
                }                                
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                if (objetoAtual != null)
                {

					if(objetoAtual.identificador == "Arma")
					{
						if(armaActual != null)
						{
							armaActual.Recargar();
						}
					}                    
                }
            }
            if (Input.GetButtonDown("Jump")) jump = true;
            

        }
        else
        {
            jump = false;
            anim.SetBool("isRunning", false);
            running = 1;
            anim.SetBool("isGrounded", false);
            anim.SetLayerWeight(1, 0);
        }
        xRoot = Input.GetAxis("Mouse X");

        state.isRunning = anim.GetBool("isRunning");
        state.isGrounded = anim.GetBool("isGrounded");
        state.Horizontal = anim.GetFloat("Horizontal");
        state.Vertical = anim.GetFloat("Vertical");
        state.GradosX = anim.GetFloat("GradosX");

		//inventario

		if (Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			inventario.CambiarObjeto(-1);
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			inventario.CambiarObjeto(1);
		}

		#endregion
		

	}

	void FixedUpdate()
    {
		if (Input.GetKeyDown(KeyCode.T))
		{
			ModificarPuntos(+50);
		}
        if (!entity.IsOwner) return;
        if (characterController.isGrounded)
        {
           
            // We are grounded, so recalculate
            // move direction directly from axes           
            

            if (jump)
            {
                moveDirection.y = jumpSpeed;
                
            }
        }
        else
        {
            anim.SetBool("isGrounded", false);
        }
        //ROTACION
        transform.Rotate(0f, xRoot * rot_speed, 0);


        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

		// Move the controller
		if (!isShopping)
		{
			characterController.Move(moveDirection * running * Time.deltaTime);
		}
		
		
        

        
    }
    public override void Attached()
    {

        state.SetTransforms(state.PosAndRoot, transform);
        state.SetAnimator(anim);

		if (entity.IsOwner)
		{
			inventario = GetComponent<Inventary>();

			FindObjectOfType<InventarioGUI>().inventaryData = inventario;

			inventario.OnInventaryChange += FindObjectOfType<InventarioGUI>().refresh;
		}
		characterController = GetComponent<CharacterController>();


	}
    public void ModificarVida(int cantidad)
    {
        vida += cantidad;
    }
	void OnTriggerStay(Collider other)
	{
		if (!entity.IsOwner) return;
		if (other.CompareTag("Tienda"))
		{
			if (Input.GetKeyDown(KeyCode.P))
			{
				isShopping = !isShopping;
				other.GetComponent<Tienda>().AbrirTienda();
				other.GetComponent<Tienda>().ConectarConPlayer(this);
			}
			
		}
	}
	public void ModificarPuntos(int cantidad)
	{
		puntos += cantidad;
	}
	public void ActivarControlador()
	{

	}
	public void DesactivarControlador()
	{
		isShopping = true;
		//buscocomponente camara y desactivo


	}
	
	



}