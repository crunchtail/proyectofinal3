﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventario : MonoBehaviour
{
    public GameObject[] objetosInventario;
    
    public Image[] objetosImagen;
    public Image[] slots;
    public Image seleccion;
    public int index = 0;
    public GameObject player;
    public Arma Act;
	InventarioGUI GUI;

    public delegate void cambioInventario();

    public event cambioInventario OnInventaryChange;

    // Start is called before the first frame update
    void Start()
    {
		GUI = FindObjectOfType<InventarioGUI>();
        objetosInventario = new GameObject[4];
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            CambiarObjeto(-1);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            CambiarObjeto(1);
        }
        Debug.Log("El indice es " + index);
        //seleccion.transform.position = Vector3.Lerp(seleccion.transform.position, slots[index].transform.position, 0.1f);
    }
    public void VoyaRecargar()
    {
        Act.RecargoElCargador();
    }
    void CambiarObjeto(int cambioArma)
    {
		if (OnInventaryChange != null) OnInventaryChange();
		if (index == 3 && cambioArma > 0)
        {
            index = 0;
        }
        else if (index == 0 && cambioArma < 0)
        {
            index = 3;
        }
        else
        {
            index += cambioArma;
        }
        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (index == i)
            {
                if (objetosInventario[i] != null)
                {
                    Debug.Log("arma seleccionada " + objetosInventario[i].gameObject);
                    objetosInventario[i].SetActive(true);
                }

            }
            else
            {
                if (objetosInventario[i] != null)
                {
                    Debug.Log("Arma Deseleccionada " + objetosInventario[i].gameObject);
                    objetosInventario[i].SetActive(false);
                }
            }
			GUI.refresh();
        }

        //seleccion.rectTransform.position = Vector2.Lerp(seleccion.rectTransform.position, objetos[index].rectTransform.position, 0.05f);

    }
    public bool AñadirAlInventario(GameObject objeto)
    {
		Debug.Log("Arma añadida");
        if(OnInventaryChange != null) OnInventaryChange();

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (objetosInventario[i] == null)
            {
                Debug.Log("Arma Añadida");
                objetosInventario[i] = objeto;
                //objetosImagen[i].sprite = objeto.GetComponent<Objeto>().imagenInventario.sprite;
				
                return true;
            }
        }
		Debug.Log("No se puede añadir el arma por que el inventario esta lleno");
        return false;
    }
    public void DropObject()
    {
        //objeto a soltar:        objetosInventario[index]
        Objeto objetoDropeable = objetosInventario[index].GetComponent<Objeto>();
        //instanciar objeto enfrente del personaje
        GameObject objetoRecolectable = Instantiate(objetoDropeable.prefabParaDropear, player.transform.position, Quaternion.identity);
        //darle fuerza para lanzar objeto

        //borrar objeto del inventario
        objetosInventario[index] = null;
    }
	public void ActualizarInventario()
	{
		for(int i = 0; i < objetosInventario.Length; i++)
		{
			if(objetosInventario[i] == null)
			{
				objetosImagen[i].sprite = null;
			}
		}
	}
	public void BorrarObjeto(Objeto item)
	{
		for(int i = 0; i < objetosInventario.Length; i++)
		{
			if(objetosInventario[i] == null) 
			{
				continue;
				
			}
			
			Objeto aux = objetosInventario[i].GetComponent<Objeto>();

			if ((aux != null) && (item.Equals(objetosInventario[i].GetComponent<Objeto>())))
			{
				
				objetosInventario[i] = null;
				objetosImagen[i].sprite = null;
				break;

			}
		}
	}
	public bool IsFull()
	{
		int contador = 0;
		for(int i = 0; i < objetosInventario.Length; i++)
		{
			if(objetosInventario[i] != null)
			{
				contador++;
			}
		}
		if(contador >= objetosInventario.Length)
		{
			return true;
		}
		return false;
	}
}
