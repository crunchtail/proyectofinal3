﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tienda : MonoBehaviour
{
	public Button ArmasButton;
	public Button ConsumiblesButton;
    public GameObject tienda;
	public GameObject armasInventario;
	public GameObject consumiblesInventario;
    private bool show;
	PlayerController Player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (show)
        {
            tienda.SetActive(true);
        }
        else
        {
            tienda.SetActive(false);
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        show = false;
    }
	public void AbrirTienda()
	{
		show = !show;
		ArmasButton.Select();
	}
	public void ShowArmas()
	{
		Debug.Log("ShowArmas");
		armasInventario.SetActive(true);
		consumiblesInventario.SetActive(false);
	}
	public void ShowConsumibles()
	{
		consumiblesInventario.SetActive(true);
		armasInventario.SetActive(false);
		
	}
	public void ComprarObjeto(GameObject prefabObjeto)
	{
		Inventario inventario = FindObjectOfType<Inventario>();
		if (!inventario.IsFull() && Player.puntos >= prefabObjeto.GetComponent<Objeto>().precio)
		{
			GameObject nuevoObjeto = Instantiate(prefabObjeto, Player.posicionObjetos);
			inventario.AñadirAlInventario(nuevoObjeto);
			Player.ModificarPuntos(-prefabObjeto.GetComponent<Objeto>().precio);
		}
		
	}
	public void ConectarConPlayer(PlayerController controlador)
	{
		Player = controlador;
	}
	
}
