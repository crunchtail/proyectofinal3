﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventary : MonoBehaviour
{
	public GameObject[] objetosInventario;

	public GameObject player;



	public int index = 0;

	InventarioGUI GUI;

	public delegate void cambioInventario();

	public event cambioInventario OnInventaryChange;
	// Start is called before the first frame update
	void Start()
    {
		player = this.gameObject;
		GUI = FindObjectOfType<InventarioGUI>();
		objetosInventario = new GameObject[4];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	public bool AñadirObjeto(GameObject objeto)
	{
		

		for (int i = 0; i < objetosInventario.Length; i++)
		{
			if (objetosInventario[i] == null)
			{
				Debug.Log("Arma Añadida");
				objetosInventario[i] = objeto;
				objeto.GetComponent<Objeto>().inventaryIndex = i;
				//objetosImagen[i].sprite = objeto.GetComponent<Objeto>().imagenInventario.sprite;
				Debug.Log(OnInventaryChange);
				//if (OnInventaryChange != null) OnInventaryChange();
				//GUI.refresh();
				ActualizarObjetos();
				return true;
			}
		}
		Debug.Log("No se puede añadir el arma por que el inventario esta lleno");
		
		//GUI.refresh();
		
		return false;
	}
	
	public void CambiarObjeto(int cambioArma)
	{
		
		if (index == 3 && cambioArma > 0)
		{
			index = 0;
		}
		else if (index == 0 && cambioArma < 0)
		{
			index = 3;
		}
		else
		{
			index += cambioArma;
		}
		ActualizarObjetos();
		

	}
	public void ActualizarObjetos()
	{
		for (int i = 0; i < objetosInventario.Length; i++)
		{
			if (index == i)
			{
				if (objetosInventario[i] != null)
				{
					Debug.Log("arma seleccionada " + objetosInventario[i].gameObject);
					objetosInventario[i].SetActive(true);
				}

			}
			else
			{
				if (objetosInventario[i] != null)
				{
					Debug.Log("Arma Deseleccionada " + objetosInventario[i].gameObject);
					objetosInventario[i].SetActive(false);
				}
			}
		}
		if (OnInventaryChange != null) OnInventaryChange();
		//GUI.refresh();
	}
	public bool IsFull()
	{
		int contador = 0;
		for (int i = 0; i < objetosInventario.Length; i++)
		{
			if (objetosInventario[i] != null)
			{
				contador++;
			}
		}
		if (contador >= objetosInventario.Length)
		{
			return true;
		}
		return false;
	}
	public void BorrarObjeto(int index)
	{
		
		Destroy(objetosInventario[index]);
		objetosInventario[index] = null;
		//GUI.refresh();
		if (OnInventaryChange != null) OnInventaryChange();
				

	}
}
