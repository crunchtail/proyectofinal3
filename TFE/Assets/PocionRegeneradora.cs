﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PocionRegeneradora : Consumible
{
	PlayerController playerController;
	bool isDrinking = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	public override void Usar(Animator anim, PlayerController controller)
	{
		if (!isDrinking)
		{
			playerController = controller;
			anim.SetTrigger("drink");
			StartCoroutine(RecuperarVida());
		}
		
		
		

	}
	IEnumerator RecuperarVida()
	{
		isDrinking = true;
		yield return new WaitForSeconds(4);
		playerController.ModificarVida(10);
		inventario.BorrarObjeto(inventaryIndex);
		
		

		//Destroy(this.gameObject);
	}
}
