﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDataManager : Bolt.EntityBehaviour<IGameManager>
{
	spawnPointsManager spawns;

	public Inventary[] inventarios = new Inventary[4];
	// Start is called before the first frame update
	void Start()
    {
		spawns = FindObjectOfType<spawnPointsManager>();
		if (BoltNetwork.IsServer)
		{
			
			entity.GetState<IGameManager>().Spawns[0] = spawns.spawnPoints[0].position;
			entity.GetState<IGameManager>().Spawns[1] = spawns.spawnPoints[1].position;
		}
		else
		{
			Destroy(spawns.gameObject);
		}
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
